import sqlite3
import pandas as pd

class ConexaoDB(object):

    conexao = None

    def __init__(self):
        users = pd.read_csv(filepath_or_buffer='movielist.csv', delimiter=';')
        conexao = self.get_conexao()
        users.to_sql('movielist', conexao, if_exists='append', index=False)

    def get_conexao(self):
        if self.conexao == None:
            self.conexao = sqlite3.connect(':memory:')
            self.conexao.text_factory = str
            self.conexao.execute('CREATE TABLE PREMIOS (year int,title text, studios text, producers text, winner boolean)')
        return self.conexao

    def close_connect(self):
        self.conexao.close()
