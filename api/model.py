class Produtores():

    def __init__(self):
        self.__lista = []

    def __len__(self):
        return len(self.__lista)

    def __getitem__(self, item):
        return self.__lista[item]

    def add_item(self, item):
        self.__lista.append(item)

    def produtor_winner_twice_more(self):
        list_min_max = {}

        for produtor in self.__lista:
            if produtor.producer not in list_min_max:
                list_min_max[produtor.producer] = {
                    "producer": produtor.producer,
                    "interval": 0,
                    "previousWin": 0,
                    "followingWin": produtor.year
                }
            else:
                list_min_max[produtor.producer]["previousWin"] = list_min_max[produtor.producer]["followingWin"]
                list_min_max[produtor.producer]["followingWin"] = produtor.year
                list_min_max[produtor.producer]["interval"] = list_min_max[produtor.producer]["followingWin"] - \
                                                              list_min_max[produtor.producer]["previousWin"]

        lista_oficial = list_min_max.copy()
        for key in list_min_max.keys():
            if list_min_max[key]["interval"] == 0:
                del lista_oficial[key]

        return lista_oficial


class Produtor():
    # year;title;studios;producers;winner
    # (year int,title text, studios text, producers text, winner boolean)

    def __init__(self, year: int, title: str, studios: str, producers: str, winner: bool):
        self.year = year
        self.title = title
        self.studio = studios
        self.producer = producers
        self.winner = True

    def __str__(self):
        return f"Title: {self.title} - Year: {self.year} - Studio: {self.studio} - Producer: {self.producer} - Won?: {self.winner}"
