from typing import Dict

from fastapi import FastAPI
from db import ConexaoDB

app = FastAPI()
conn = ConexaoDB().get_conexao()


@app.get("/producer/winner")
async def get_producer_winner():
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM movielist where winner = "yes" order by producers,year ')
    result = cursor.fetchall()
    min = 999
    max = -999
    dict_prod = {}
    for item in result:
        (year, title, studio, producer, winner) = item
        if producer in dict_prod and "years" in dict_prod[producer]:
            dict_prod[producer]["years"].append(year)
        else:
            dict_prod.update({producer: {"years": [year]}})
        dmin, dmax = define_minimo_maximo(dict_prod[producer]["years"])
        if dmin:
            dict_prod[producer]["min"] = dmin
            if dmin["min"] < min:
                min = dmin["min"]
        if dmax:
            dict_prod[producer]["max"] = dmax
            if dmax["max"] > max:
                max = dmax["max"]

    out = {
        "min": [],
        "max": []
    }
    for item in dict_prod:
        if "min" in dict_prod[item]:
            if dict_prod[item]["min"]["min"] == min:
                out["min"].append({
                    "producer": item,
                    "interval": dict_prod[item]["min"]["min"],
                    "previousWin": dict_prod[item]["min"]["previous"],
                    "followingWin": dict_prod[item]["min"]["last"]
                })
        if "max" in dict_prod[item]:
            if dict_prod[item]["max"]["max"] == max:
                out["max"].append({
                    "producer": item,
                    "interval": dict_prod[item]["max"]["max"],
                    "previousWin": dict_prod[item]["max"]["previous"],
                    "followingWin": dict_prod[item]["max"]["last"]
                })

    return out


def define_minimo_maximo(dict_prod: list):
    if len(dict_prod) == 1:
        return {}, {}
    lista_ordenada = dict_prod.copy()
    lista_ordenada.sort()
    min = 99
    max = -1
    dict_min = {}
    dict_max = {}

    for index, item in enumerate(lista_ordenada[:-1]):
        diff = lista_ordenada[index + 1] - item
        if diff < min:
            min = diff
            dict_min["min"] = min
            dict_min["previous"] = item
            dict_min["last"] = lista_ordenada[index + 1]

    if lista_ordenada[-1] - lista_ordenada[0] > max:
        max = lista_ordenada[-1] - lista_ordenada[0]

        dict_max["max"] = max
        dict_max["previous"] = lista_ordenada[0]
        dict_max["last"] = lista_ordenada[-1]

    return dict_min, dict_max
